package com.dodo.model;

import org.eclipse.microprofile.openapi.annotations.media.Schema;

import javax.validation.constraints.Min;
import javax.validation.constraints.NotBlank;
import javax.validation.constraints.NotNull;
import javax.ws.rs.DefaultValue;
import java.util.UUID;

/*
Parent class and in this class we validate all the elements
Also here we use uuid not id
 */
public class Person {
    @NotNull
    private UUID uuid = UUID.randomUUID();
    @NotNull
    @NotBlank(message = "First name cannot empty")
    @Schema(required = true, example = "Rivaldo")
    private String firstName;
    @NotNull
    @NotBlank(message = "Last name cannot empty")
    @Schema(required = true, example = "Lawalata")
    private String lastName;
    @Min(message = "Age cannot under 18 years old", value = 18)
    @NotNull(message = "Age cannot null")
    @Schema(required = true, example = "21")
    private Integer age;

    public UUID getUuid() {
        return uuid;
    }

    public void setUuid(UUID uuid) {
        this.uuid = uuid;
    }

    public String getFirstName() {
        return firstName;
    }

    public void setFirstName(String firstName) {
        this.firstName = firstName;
    }

    public String getLastName() {
        return lastName;
    }

    public void setLastName(String lastName) {
        this.lastName = lastName;
    }

    public Integer getAge() {
        return age;
    }

    public void setAge(Integer age) {
        this.age = age;
    }
}
