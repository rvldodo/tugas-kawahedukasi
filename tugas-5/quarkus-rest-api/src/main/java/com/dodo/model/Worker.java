package com.dodo.model;

import org.eclipse.microprofile.openapi.annotations.media.Schema;

import javax.validation.constraints.Min;
import javax.validation.constraints.NotBlank;
import javax.validation.constraints.NotNull;
import javax.ws.rs.DefaultValue;
import java.text.NumberFormat;
import java.util.Locale;

/*
This is a subclass of Person class and in this class we add other elements such as occupation, company and yearExperience.
Still working with the salary because I want to change the currency format but still got error
 */
public class Worker extends Person {
    @NotNull
    @NotBlank(message = "Occupation cannot empty")
    @Schema(required = true, example = "Backend-developer")
    private String occupation;
    @Schema(required = true, example = "Kawahedukasi")
    private String company;
    @Schema(required = true, example = "3")
    @Min(value = 0, message = "Years experience cannot negative")
    private Integer yearsExperience;
    @Schema(required = true, example = "9000000")
    @Min(value = 0, message = "Salary cannot negative")
    private Integer salary;
    // private String salary;

    public Worker() {

    }

    public String getOccupation() {
        return occupation;
    }

    public void setOccupation(String occupation) {
        this.occupation = occupation;
    }

    public String getCompany() {
        // check if the company name is blank or not
        if(company.isBlank() || company == null) {
            return company = "-";
        } else {
            return company;
        }
    }

    public void setCompany(String company) {
        this.company = company;
    }

    // here 0 means haven't an experience
    public Integer getYearsExperience() {
        // check if the yearsExperience is blank or not
        if(yearsExperience == null || String.valueOf(yearsExperience).isBlank()) {
            return yearsExperience = 0;
        } else {
            return yearsExperience;
        }
    }

    public void setYearsExperience(Integer yearsExperience) {
        this.yearsExperience = yearsExperience;
    }

    public Integer getSalary() {
        if(salary == null || String.valueOf(salary).isBlank()) {
            return salary = 0;
        } else {
            return salary;
        }
    }

    public void setSalary(Integer salary) {
        this.salary = salary;
    }

    /*
    public String getSalaryCurrency() {
        Locale locale = Locale.US;
        NumberFormat formatter = NumberFormat.getCurrencyInstance(locale);
        return formatter.format(Integer.valueOf(salary));
    }

    public void setSalaryCurrency(Integer salary) {
        this.salary = String.valueOf(salary);
    }
    */

}
