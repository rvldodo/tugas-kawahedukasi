package com.dodo;

import com.dodo.model.Worker;

import javax.inject.Inject;
import javax.validation.ConstraintViolation;
import javax.validation.ConstraintViolationException;
import javax.validation.Valid;
import javax.validation.Validator;
import javax.ws.rs.*;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;
import java.util.*;
import java.util.stream.Collectors;

@Path("data")
public class DataResources {

    @Inject
    Validator validator;

    Map<UUID, Worker> workers = new HashMap<>();

    // get all the data created
    @GET
    @Produces(MediaType.APPLICATION_JSON)
    @Consumes(MediaType.APPLICATION_JSON)
    public Response getAllPeople(){
        return Response.ok(workers).build();
    }

    // get a specific data by uuid
    @GET
    @Path("{uuid}")
    public Response getCurrentPerson(UUID uuid) {
        Worker person = workers.get(uuid);
        return Response.ok(person).build();
    }

    // post or create a data and make a validation
    @POST
    @Path("worker")
    @Consumes(MediaType.APPLICATION_JSON)
    @Produces(MediaType.APPLICATION_JSON)
    public Result createWorker(@Valid Worker worker){
        Set<ConstraintViolation<Worker>> violations = validator.validate(worker);
        try{
            workers.put(worker.getUuid(), worker);
//            return Response.ok(worker).build();
            return new Result("Data created");
        } catch (ConstraintViolationException e) {
            return new Result(violations.stream().map(cv -> cv.getMessage()).collect(Collectors.joining(", ")));
//            return Response.status(400).build();
        }
    }

    // update the data uuid and validate it
    @PUT
    @Path("{uuid}")
    @Produces(MediaType.APPLICATION_JSON)
    @Consumes(MediaType.APPLICATION_JSON)
    public Result updatePerson(UUID uuid, @Valid Worker newData){
        Worker worker = workers.get(uuid);
        Set<ConstraintViolation<Worker>> violations = validator.validate(newData);
        try{
            workers.replace(uuid, newData);
//            worker.setFirstName(newData.getFirstName());
//            worker.setLastName(newData.getLastName());
//            worker.setAge(newData.getAge());
//            worker.setOccupation(newData.getOccupation());
//            worker.setCompany(newData.getCompany());
//
//            // update in key salary still doesn't work
//            // worker.setSalary(Integer.valueOf(newData.getSalary()));
//            worker.setSalary(newData.getSalary());
//
//            worker.setYearsExperience(newData.getYearsExperience());
            return new Result("Data updated");
//            return Response.ok(worker).build();
        }catch (ConstraintViolationException e){
             return new Result(violations.stream().map(cv -> cv.getMessage()).collect(Collectors.joining(", ")));
//            return Response.status(400, "Cannot update data").build();
        }
    }

    // delete a data by uuid
    @DELETE
    @Path("{uuid}")
    @Produces(MediaType.APPLICATION_JSON)
    @Consumes(MediaType.APPLICATION_JSON)
    public Result deleteWorker(UUID uuid) {
        try{
            workers.remove(uuid);
            return new Result("Data with id: "  + uuid + " successfully deleted");
        } catch (ConstraintViolationException e) {
            return new Result("Data with id: " + uuid + " cannot found");
        }
    }

    // delete all the data
    @DELETE
    @Path("all")
    @Produces(MediaType.APPLICATION_JSON)
    @Consumes({MediaType.APPLICATION_JSON})
    public Result deleteAllWorkers(){
        try{
            workers.clear();
            return new Result("Successfully deleted all data");
        } catch (ConstraintViolationException e) {
            return new Result("Cannot delete all the data");
        }

    }

    /*
    * VALIDATOR*/
    public static class Result{
        private String message;
        private Boolean success;

        public Result(String message) {
            this.message = message;
            this.success = true;
        }

        public Result(Set<? extends ConstraintViolation<?>> violations) {
            this.success = false;
            this.message = violations.stream().map(cv -> cv.getMessage()).collect(Collectors.joining(", "));
        }

        public String getMessage() {
            return message;
        }

        public Boolean getSuccess() {
            return success;
        }
    }

}
