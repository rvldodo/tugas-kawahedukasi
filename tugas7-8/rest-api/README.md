# RestAPI

## Entities

### Employee

- UUID
- First name
- Last name
- Role
- Project(many-to-many)
- Personal Information(one-to-one)
- Department(one-to-one)

### Project

- UUID
- Project name
- Technology

### Department

- UUID
- Department name
- Company(many-to-one)

### Personal Information

- UUID
- Full name
- Age
- Address
- Place of born
- Date-of-born (DOB)
- Salary
- Experience

### Company

- UUID
- Company name
- Main field
- Department(one-to-many)

## Association

1. one-to-one(employee-personalInformation, employee-department)
2. one-to-many(company-department)
3. many-to-many(employee-project)

## Resources & Method

1. Employee Resource
   - GET -> All
   - GET -> By UUID
   - POST
   - DELETE
2. Project Resource
   - GET -> All
   - GET -> By UUID
   - POST
   - DELETE
3. Department Resource
   - GET -> All
   - GET -> By UUID
   - POST
   - DELETE
4. Personal Information Resource
   - GET -> All
   - GET -> By UUID
   - PUT
   - DELETE
5. Company Resouce
   - GET -> All
   - GET -> By UUID
   - POST

## Response Template

1. **Employee**

   ```aidl
   {
      "status": "status",
      "message": "message",
      "object" :[
                  {
                   "uuid" :,
                   "firstName" :,
                   "lastName" :,
                   "role" :,
                   "department" : {Object},
                   "personalInformation" : {Object},
                   "project" : {Object},
                  }
      ]
   }

   ```

2. **Project**

   ```aidl
   {
      "status": "status",
      "message": "message",
      "object": [
                   {
                      "uuid":,
                      "projectName":,
                      "technologies":,
                   }
      ]
   }


   ```

3. **Department**
   ```aidl
   {
      "status": "status",
      "message": "message",
      "object": [
                  {
                    "uuid":,
                    "department":
                  }
      ]
   }
   ```
4. **Personal Information**
   ```aidl
   {
      "status": "status",
      "message": "message",
      "object": [
                  {
                      "uuid":,
                      "age":,
                      "address":,
                      "placeOfBorn":,
                      "dob":,
                      "salary":,
                      "experience" :
                   }
      ]
   }
   ```
5. **Company**
   ```aidl
   {
     "status": "status",
     "message": "message",
     "object": [
                 {
                   "uuid":,
                   "companyName":,
                   "mainField":
                 }
     ]
   }
   ```

## Dependecies

```aidl
✬ quarkus-hibernate-orm-panache                      Hibernate ORM with Panache
✬ quarkus-hibernate-validator                        Hibernate Validator
✬ quarkus-jdbc-postgresql                            JDBC Driver - PostgreSQL
✬ quarkus-resteasy-reactive                          RESTEasy Reactive
✬ quarkus-resteasy-reactive-jackson                  RESTEasy Reactive Jackson
✬ quarkus-smallrye-openapi                           SmallRye OpenAPI
```
