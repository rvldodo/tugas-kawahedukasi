package com.dodo.responeTemplate;

public class ResponseTemplate {
    private Integer success;
    private String message;
    private Object object;

    public ResponseTemplate(Integer success, String message, Object object) {
        this.success = success;
        this.message = message;
        this.object = object;
    }

    public Integer getSuccess() {
        return success;
    }

    public void setSuccess(Integer success) {
        this.success = success;
    }

    public String getMessage() {
        return message;
    }

    public void setMessage(String message) {
        this.message = message;
    }

    public Object getObject() {
        return object;
    }

    public void setObject(Object object) {
        this.object = object;
    }
}
