package com.dodo.resources;

import com.dodo.entities.PersonalInformation;
import com.dodo.responeTemplate.ResponseTemplate;
import io.quarkus.hibernate.orm.panache.PanacheEntityBase;

import javax.transaction.Transactional;
import javax.ws.rs.*;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;
import java.util.List;
import java.util.UUID;

@Path("info")
public class PersonalInformationResource extends PanacheEntityBase {
    @GET
    @Consumes(MediaType.APPLICATION_JSON)
    @Produces(MediaType.APPLICATION_JSON)
    public List<PersonalInformation> getAll(){
        List<PersonalInformation> infos = PersonalInformation.listAll();

        return infos;
    }

    @GET
    @Consumes(MediaType.APPLICATION_JSON)
    @Produces(MediaType.APPLICATION_JSON)
    @Path("{uuid}")
    public PersonalInformation getByUuid(UUID uuid) {
        return PersonalInformation.findById(uuid);
    }

    @PUT
    @Path("{uuid}/employee")
    @Produces(MediaType.APPLICATION_JSON)
    @Consumes(MediaType.APPLICATION_JSON)
    @Transactional
    public ResponseTemplate updatePersonalInfo(UUID uuid, PersonalInformation newInfo) {
        PersonalInformation oldInfo = PersonalInformation.findById(uuid);

        oldInfo.setFullName(newInfo.getFullName());
        oldInfo.setAge(newInfo.getAge());
        oldInfo.setAddress(newInfo.getAddress());
        oldInfo.setDob(newInfo.getDob());
        oldInfo.setPlaceBorn(newInfo.getPlaceBorn());
        oldInfo.setSalary(newInfo.getSalary());
        oldInfo.setExperience(newInfo.getExperience());

        return new ResponseTemplate(200, "Success update Personal Information", oldInfo);
    }

    @DELETE
    @Path("{uuid}")
    @Produces(MediaType.APPLICATION_JSON)
    @Consumes(MediaType.APPLICATION_JSON)
    @Transactional
    public Response deleteInfo(UUID uuid) {
        boolean deleted = PersonalInformation.deleteById(uuid);
        List<PersonalInformation> informations = PersonalInformation.listAll();

        if(deleted) {
            return Response.ok(informations).build();
        }

        return Response.status(Response.Status.NOT_FOUND).build();
    }
}

