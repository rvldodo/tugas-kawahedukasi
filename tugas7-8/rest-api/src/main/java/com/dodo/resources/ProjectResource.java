package com.dodo.resources;

import com.dodo.entities.Employee;
import com.dodo.entities.Project;
import com.dodo.responeTemplate.ResponseTemplate;

import javax.transaction.Transactional;
import javax.ws.rs.*;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;
import java.util.List;
import java.util.UUID;

@Path("project")
public class ProjectResource {
    @GET
    @Produces(MediaType.APPLICATION_JSON)
    @Consumes(MediaType.APPLICATION_JSON)
    public List<Project> getAll() {
        return Project.listAll();
    }

    @GET
    @Path("{projectUuid}")
    @Produces(MediaType.APPLICATION_JSON)
    @Consumes(MediaType.APPLICATION_JSON)
    public Project getByUuid(UUID projectUuid) {
        return Project.findById(projectUuid);
    }

    @POST
    @Path("{employeeUuid}/employee")
    @Transactional
    @Produces(MediaType.APPLICATION_JSON)
    @Consumes(MediaType.APPLICATION_JSON)
    public ResponseTemplate createProject(UUID employeeUuid, Project project) {
        // first, save the project created
        project.persist();

        // check if the project already save or not?
        if(project.isPersistent()){
            // if already save, then find the employee by uuid
            Employee employee = Employee.findById(employeeUuid);
            // add project to the project attribute in employee
            employee.getProject().add(project);
        }

        return new ResponseTemplate(200, "Success create project", project);
    }

    @DELETE
    @Produces(MediaType.APPLICATION_JSON)
    @Consumes(MediaType.APPLICATION_JSON)
    @Transactional
    @Path("{employeeUuid}/{projectUuid}")
    public Response deleteProject(UUID employeeUuid, UUID projectUuid) {
        Employee employee = Employee.findById(employeeUuid);
        Project project = Project.findById(projectUuid);

        employee.getProject().remove(project);
        Project.deleteById(projectUuid);

        return Response.status(200, "Successfully deleted project").build();
    }
}
