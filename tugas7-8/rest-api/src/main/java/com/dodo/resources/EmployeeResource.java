package com.dodo.resources;

import com.dodo.entities.Department;
import com.dodo.entities.Employee;
import com.dodo.responeTemplate.ResponseTemplate;

import javax.transaction.Transactional;
import javax.ws.rs.*;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;
import java.util.List;
import java.util.UUID;

@Path("employee")
public class EmployeeResource {
    @GET
    @Produces(MediaType.APPLICATION_JSON)
    @Consumes(MediaType.APPLICATION_JSON)
    public List<Employee> getAll() {
        return Employee.listAll();
    }

    @GET
    @Path("{uuid}")
    @Produces(MediaType.APPLICATION_JSON)
    @Consumes(MediaType.APPLICATION_JSON)
    public Employee getSingle(UUID uuid){
        return Employee.findById(uuid);
    }

    @POST
    @Produces(MediaType.APPLICATION_JSON)
    @Consumes(MediaType.APPLICATION_JSON)
    @Transactional
    @Path("{departmentUuid}")
    public ResponseTemplate createEmployee (UUID departmentUuid, Employee employee){
        // First we find the department for create an employee
        Department department = Department.findById(departmentUuid);

        // then, set the department in employee attribute to the department find by uuid
        employee.setDepartment(department);
        // finally, save the employee
        employee.persist();
        return new ResponseTemplate(200, "Success create an employee", employee);
    }


    @DELETE
    @Consumes(MediaType.APPLICATION_JSON)
    @Produces(MediaType.APPLICATION_JSON)
    @Transactional
    @Path("{employeeUuid}")
    public Response deleteEmployee(UUID employeeUuid){
        boolean deleted = Employee.deleteById(employeeUuid);
        if(deleted) {
            return Response.status(200, "Employee with id " + employeeUuid + " successfully deleted").build();
        }
        return Response.status(Response.Status.NOT_FOUND).build();
    }
}
