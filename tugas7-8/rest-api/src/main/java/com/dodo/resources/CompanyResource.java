package com.dodo.resources;

import com.dodo.entities.Company;
import com.dodo.responeTemplate.ResponseTemplate;
import io.quarkus.hibernate.orm.panache.PanacheEntityBase;

import javax.inject.Inject;
import javax.persistence.EntityManager;
import javax.transaction.Transactional;
import javax.ws.rs.*;
import javax.ws.rs.core.MediaType;
import java.util.List;
import java.util.UUID;

@Path("company")
public class CompanyResource extends PanacheEntityBase {
    @Inject
    EntityManager entityManager;
    @GET
    @Produces(MediaType.APPLICATION_JSON)
    @Consumes(MediaType.APPLICATION_JSON)
    public List<Company> getAll() {
        return Company.listAll();
    }

    @GET
    @Produces(MediaType.APPLICATION_JSON)
    @Consumes(MediaType.APPLICATION_JSON)
    @Path("{uuid}")
    public Company getByUuid(UUID uuid) {
        return Company.findById(uuid);
    }

    @POST
    @Produces(MediaType.APPLICATION_JSON)
    @Consumes(MediaType.APPLICATION_JSON)
    @Transactional
    public ResponseTemplate createCompany(Company company){
        // create and save the company
        company.persist();

        return new ResponseTemplate(200, "Success create a company", company);
    }
}