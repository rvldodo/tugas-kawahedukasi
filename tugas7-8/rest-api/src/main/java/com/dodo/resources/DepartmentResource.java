package com.dodo.resources;

import com.dodo.entities.Company;
import com.dodo.entities.Department;
import com.dodo.responeTemplate.ResponseTemplate;

import javax.inject.Inject;
import javax.persistence.EntityManager;
import javax.transaction.Transactional;
import javax.ws.rs.*;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;
import java.util.List;
import java.util.UUID;

@Path("department/{companyUuid}")
public class DepartmentResource {
    @Inject
    EntityManager entityManager;
    @GET
    @Produces(MediaType.APPLICATION_JSON)
    @Consumes(MediaType.APPLICATION_JSON)
    public List<Department> getAll() {
        return Department.listAll();
    }

    @GET
    @Produces(MediaType.APPLICATION_JSON)
    @Consumes(MediaType.APPLICATION_JSON)
    @Path("{departmentUuid}")
    public Department getByUuid(UUID departmentUuid) {
        return Department.findById(departmentUuid);
    }

    @POST
    @Produces(MediaType.APPLICATION_JSON)
    @Consumes(MediaType.APPLICATION_JSON)
    @Transactional
    public ResponseTemplate createDepartment(@PathParam("companyUuid") UUID companyUuid, Department department){
        // find the company by uuid
        Company company = Company.findById(companyUuid);
        // set the company in department attribute with company we find by uuid
        department.setCompany(company);
        // and save the department
        department.persist();

        return new ResponseTemplate(200, "Success create a department", department);
    }

    @DELETE
    @Produces(MediaType.APPLICATION_JSON)
    @Consumes(MediaType.APPLICATION_JSON)
    @Transactional
    @Path("{departmentUuid}")
    public Response deleteDepartment(UUID departmentUuid) {
        boolean deleted = Department.deleteById(departmentUuid);
        List<Department> departments = Department.listAll();

        if(deleted) {
            return Response.ok(departments).build();
        }

        return Response.status(Response.Status.NOT_FOUND).build();
    }
}
