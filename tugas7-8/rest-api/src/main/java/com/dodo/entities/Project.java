package com.dodo.entities;

import io.quarkus.hibernate.orm.panache.PanacheEntityBase;
import org.eclipse.microprofile.openapi.annotations.media.Schema;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.validation.constraints.NotBlank;
import javax.validation.constraints.NotNull;
import java.util.UUID;

@Entity
public class Project extends PanacheEntityBase {

    /*
    Used the private access modifier so it can't be access directly, but don't forget to create getter and setter
     */
    @Id
    @GeneratedValue
    @Schema(readOnly = true)
    private UUID uuid;
    @Schema(example = "Kamarpelajar")
    @Column(name = "project_name")
    @NotNull
    private String projectName;
    @Schema(example = "PHP, Laravel, MySQL")
    @NotBlank
    private String technologies;

    public UUID getUuid() {
        return uuid;
    }

    public void setUuid(UUID uuid) {
        this.uuid = uuid;
    }

    public String getProjectName() {
        return projectName;
    }

    public void setProjectName(String projectName) {
        this.projectName = projectName;
    }

    public String getTechnologies() {
        return technologies;
    }

    public void setTechnologies(String technologies) {
        this.technologies = technologies;
    }
}