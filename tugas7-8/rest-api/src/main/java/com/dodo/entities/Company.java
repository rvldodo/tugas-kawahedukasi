package com.dodo.entities;

import com.fasterxml.jackson.annotation.JsonIgnore;
import io.quarkus.hibernate.orm.panache.PanacheEntityBase;
import org.eclipse.microprofile.openapi.annotations.media.Schema;

import javax.persistence.*;
import javax.validation.constraints.NotBlank;
import java.util.List;
import java.util.UUID;

@Entity
public class Company extends PanacheEntityBase {
    /*
    Used the private access modifier so it can't be access directly, but don't forget to create getter and setter
     */
    @Id
    @GeneratedValue
    @Schema(readOnly = true)
    private UUID uuid;
    @Schema(example = "Java Express")
    @Column(name = "company_name")
    @NotBlank
    private String companyName;
    @Schema(example = "Technology")
    @Column(name = "main_field")
    @NotBlank
    private String mainField;
    /*
    For every company always have many departments so it's a one-to-many association
     */
    @OneToMany(mappedBy = "company")
    @Schema(readOnly = true)
    private List<Department> departments;

    public UUID getUuid() {
        return uuid;
    }

    public void setUuid(UUID uuid) {
        this.uuid = uuid;
    }

    public String getCompanyName() {
        return companyName;
    }

    public void setCompanyName(String companyName) {
        this.companyName = companyName;
    }

    public String getMainField() {
        return mainField;
    }

    public void setMainField(String mainField) {
        this.mainField = mainField;
    }

    public List<Department> getDepartments() {
        return departments;
    }

    @JsonIgnore
    public void setDepartments(List<Department> departments) {
        this.departments = departments;
    }
}
