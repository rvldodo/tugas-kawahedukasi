package com.dodo.entities;

import io.quarkus.hibernate.orm.panache.PanacheEntityBase;
import org.eclipse.microprofile.openapi.annotations.media.Schema;

import javax.persistence.*;
import javax.validation.constraints.NotNull;
import java.util.List;
import java.util.UUID;

@Entity
public class Employee extends PanacheEntityBase {
    /*
    Used the private access modifier so it can't be access directly, but don't forget to create getter and setter
     */
    @Id
    @Schema(readOnly = true)
    @GeneratedValue
    private UUID uuid;
    @Schema(example = "Rivaldo")
    @Column(name = "first_name")
    @NotNull
    private String firstName;
    @Schema(example = "Lawalata")
    @Column(name = "last_name")
    @NotNull
    private String lastName;
    @Schema(example = "Backend-developer")
    private String role;

    /*
    Personal Information is one-to-one association
    because, every employees only have one personal data information
     */
    @OneToOne(cascade = CascadeType.ALL)
    @JoinColumn(name = "personal_info_uuid", referencedColumnName = "uuid")
    private PersonalInformation personalInformation;

    /*
    Each employees can work with many project so it's many-to-many association
     */
    @ManyToMany
    @JoinTable(name = "employee_project", joinColumns = @JoinColumn(name = "employee_uuid", referencedColumnName = "uuid"), inverseJoinColumns = @JoinColumn(name = "project_uuid", referencedColumnName = "uuid"))
    @Schema(readOnly = true)
    private List<Project> project;

    /*
    Each employees only have to work in one department in a company and each departments can have many employees
     */
    @ManyToOne
    @JoinColumn(name = "department_uuid", referencedColumnName = "uuid")
    @Schema(readOnly = true)
    private Department department;

    public String getFirstName() {
        return firstName;
    }

    public void setFirstName(String firstName) {
        this.firstName = firstName;
    }

    public String getLastName() {
        return lastName;
    }

    public void setLastName(String lastName) {
        this.lastName = lastName;
    }

    public String getRole() {
        return role;
    }

    public void setRole(String role) {
        this.role = role;
    }

    public UUID getUuid() {
        return uuid;
    }

    public void setUuid(UUID uuid) {
        this.uuid = uuid;
    }

    public PersonalInformation getPersonalInformation() {
        return personalInformation;
    }

    public void setPersonalInformation(PersonalInformation personalInformation) {
        this.personalInformation = personalInformation;
    }

    public List<Project> getProject() {
        return project;
    }

    public void setProject(List<Project> project) {
        this.project = project;
    }

    public Department getDepartment() {
        return department;
    }

    public void setDepartment(Department department) {
        this.department = department;
    }
}
