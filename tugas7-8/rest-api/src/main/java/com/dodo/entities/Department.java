package com.dodo.entities;

import io.quarkus.hibernate.orm.panache.PanacheEntityBase;
import org.eclipse.microprofile.openapi.annotations.media.Schema;

import javax.persistence.*;
import javax.validation.constraints.NotBlank;
import java.util.List;
import java.util.UUID;

@Entity
public class Department extends PanacheEntityBase {
    /*
    Used the private access modifier so it can't be access directly, but don't forget to create getter and setter
     */
    @Id
    @Schema(readOnly = true)
    @GeneratedValue
    private UUID uuid;
    @Schema(example = "IT Research")
    @NotBlank
    private String departmentName;
    /*
    Every departments always in every company so it's many-to-one association
     */
    @ManyToOne
    @JoinColumn(name = "company_uuid", referencedColumnName = "uuid", nullable = false)
    @Schema(readOnly = true)
    private Company company;

    /*
    In a departments it's allow to have many employees so it's a one-to-many association
     */
    @OneToMany(mappedBy = "department")
    @Schema(readOnly = true)
    private List<Employee> employees;

    public Company getCompany() {
        return company;
    }

    public void setCompany(Company company) {
        this.company = company;
    }

    public UUID getUuid() {
        return uuid;
    }

    public void setUuid(UUID uuid) {
        this.uuid = uuid;
    }

    public String getDepartmentName() {
        return departmentName;
    }

    public void setDepartmentName(String departmentName) {
        this.departmentName = departmentName;
    }
}
