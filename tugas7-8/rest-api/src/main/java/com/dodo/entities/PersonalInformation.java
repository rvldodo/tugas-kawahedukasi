package com.dodo.entities;

import io.quarkus.hibernate.orm.panache.PanacheEntityBase;
import org.eclipse.microprofile.openapi.annotations.media.Schema;

import javax.persistence.*;
import javax.validation.constraints.Min;
import javax.validation.constraints.NotNull;
import java.time.LocalDate;
import java.util.UUID;

@Entity
@Table(name = "personal_info")
public class PersonalInformation extends PanacheEntityBase {
    /*
    Used the private access modifier so it can't be access directly, but don't forget to create getter and setter
     */
    @Id
    @Schema(readOnly = true)
    @GeneratedValue
    private UUID uuid;
    @Column(name = "full_name")
    @Schema(example = "Rivaldo Ardika Lawalata")
    private String fullName;
    @Schema(example = "21")
    @Min(message = "Age must over 18 years old", value = 18)
    private Integer age;
    @Schema(example = "Adelya kutuya 37, Kazan, Russia")
    @NotNull
    private String address;
    @Schema(example = "Ambon, Indonesia")
    @Column(name = "place_born")
    @NotNull
    private String placeBorn;
    @Schema(example = "2001-03-28")
    @Column(name = "date_of_born")
    private LocalDate dob;
    @Schema(example = "7000000")
    @NotNull
    private Integer salary;
    @Schema(example = "2")
    @Min(message = "Cannot negative value", value = 0)
    private Integer experience;

    public UUID getUuid() {
        return uuid;
    }

    public void setUuid(UUID uuid) {
        this.uuid = uuid;
    }

    public Integer getAge() {
        return age;
    }

    public void setAge(Integer age) {
        this.age = age;
    }

    public String getAddress() {
        return address;
    }

    public void setAddress(String address) {
        this.address = address;
    }

    public String getPlaceBorn() {
        return placeBorn;
    }

    public void setPlaceBorn(String placeBorn) {
        this.placeBorn = placeBorn;
    }

    public LocalDate getDob() {
        return dob;
    }

    public void setDob(LocalDate dob) {
        this.dob = dob;
    }

    public Integer getSalary() {
        return salary;
    }

    public void setSalary(Integer salary) {
        this.salary = salary;
    }

    public String getFullName() {
        return fullName;
    }

    public void setFullName(String fullName) {
        this.fullName = fullName;
    }

    public Integer getExperience() {
        return experience;
    }

    public void setExperience(Integer experience) {
        this.experience = experience;
    }
}
