import java.util.Arrays;
import java.util.Scanner;

public class TugasDua {

    /** The methods
     * This is the list of methods
     * The first method, is for searching the absolute number of the input number
     * The second method, is for searching the maximum number of an array
     * The third method, is for searching the power number
     * 
     * PS: All the methods aren't using the method from java.util.Math
    */ 

    // The Absolute Method
    public static int absoluteMethode(int number) {
        int absNumber = (number < 0) ? -number : number;
        return absNumber;
    }

    // The Power Method
    public static int powerNumber(int num1, int num2) {
        int res = 1;
        while(num2 != 0) {
            res *= num1;
            num2--;
        }
        return res;
    }

    // The Maximum Number of Array Method
    public static int maxNumberArray(int[] number) {
        int max = 0;
        for (int i = 0; i < number.length; i++) {
            if(number[i] >= max) {
                max = number[i];
            }
        }
        return max;
    }

    // Method for printing the questions and return the value
    public static int inputArray() {
        Scanner scanner = new Scanner(System.in);

        System.out.print("Enter the length of the array: ");
        int size = scanner.nextInt();
        scanner.nextLine();
        int[] myArray = new int[size];

        for(int i = 0; i < myArray.length; i++) {
            System.out.print("Enter the element: ");
            myArray[i] = scanner.nextInt();
            scanner.nextLine();
        }
        System.out.println("The array before sorted: " + Arrays.toString(myArray));
        // here we sorted the array
        Arrays.sort(myArray);
        System.out.println("The array after sorted: " + Arrays.toString(myArray));
        return maxNumberArray(myArray);
    }

    // Method for print all the description and value
    public static void methodOne() {
        Scanner scanner = new Scanner(System.in);

        System.out.println("Absolute Number");
        System.out.println("-----------------");
        System.out.println("This is a absolute method where you can input a number and search for it's absolute value");
        System.out.print("Input a number: ");
        int number = scanner.nextInt();
        scanner.nextLine();
        System.out.println("The absolute value of " + number + " is " +  absoluteMethode(number));
        System.out.println();
        System.out.println("9. Back");
        System.out.println("1. Restart");
        System.out.println("0. Exit");
        System.out.print("Input menu: ");
        int menu = scanner.nextInt();
        scanner.nextLine();
        System.out.println();

        if(menu == 9) {
            // recursive call
            mainMenu();
        } else if(menu == 0) {
            System.exit(0);
        } else if(menu == 1) {
            // recursive call
            methodOne();
        } else {
            System.out.println("Invalid input\n");
            // recursive call
            mainMenu();
        }
    }

    // Method for print all the description and value
    public static void methodTwo() {
        Scanner scanner = new Scanner(System.in);

        System.out.println("Maximum Number of Array");
        System.out.println("-----------------");
        System.out.println("This is a maximum number method of array elements where you can input an array of numbers and find the maximum number of the array");
        System.out.println("The maximum number: " + inputArray());
        System.out.println();
        System.out.println("9. Back");
        System.out.println("1. Restart");
        System.out.println("0. Exit");
        System.out.print("Input menu: ");
        int menu = scanner.nextInt();
        scanner.nextLine();
        System.out.println();

        if(menu == 9) {
            // recursive call
            mainMenu();
        } else if(menu == 0) {
            System.exit(0);
        } else if(menu == 1) {
            // recursive call
            methodTwo();
        } else {
            System.out.println("Invalid input\n");
            // recursive call
            mainMenu();
        }
    }

    // Method for print all the description and value
    public static void methodThree() {
        Scanner scanner = new Scanner(System.in);

        System.out.println("Power Number");
        System.out.println("-----------------");
        System.out.println("This is a power number method where you can input 2 numbers, the first number will become the base number and the second number will become the power number");
        System.out.print("Input number 1: ");
        int number1 = scanner.nextInt();
        scanner.nextLine();
        System.out.print("Input number 2: ");
        int number2 = scanner.nextInt();
        scanner.nextLine();
        System.out.println("Result: " + powerNumber(number1, number2));
        System.out.println();
        System.out.println("9. Back");
        System.out.println("1. Restart");
        System.out.println("0. Exit");
        System.out.print("Input menu: ");
        int menu = scanner.nextInt();
        scanner.nextLine();
        System.out.println();

        if(menu == 9) {
            // recursive call
            mainMenu();
        } else if(menu == 0) {
            System.exit(0);
        } else if(menu == 1) {
            // recursive call
            methodThree();
        } else {
            System.out.println("Invalid input\n");
            // recursive call
            mainMenu();
        }
    }

    // Method for print all the description
    public static void mainMenu() {
        Scanner scanner = new Scanner(System.in);

        System.out.println("MENU");
        System.out.println("==========");
        System.out.println("1. Absolute number");
        System.out.println("2. Maximum number of array");
        System.out.println("3. Power number");
        System.out.println();
        System.out.println("0. Exit");
        System.out.print("Select a method: ");
        int input = scanner.nextInt();
        scanner.nextLine();
        System.out.println();

        if(input == 1) {
            methodOne();
        } else if (input == 2) {
            methodTwo();
        } else if(input == 3) {
            methodThree();
        } else if(input == 0) {
            System.exit(0);
        } else {
            System.out.println("Invalid input\n");
            mainMenu();
        }
    }

    // The main method for run the program
    public static void main(String[] args) {
        mainMenu();
    }
}