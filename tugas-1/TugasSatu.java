import java.util.Scanner;

public class TugasSatu {
    public static void main(String[] args)  {
        menu();
    }

    // create a menu method  to choose 3 types of operations
    public static void menu() {
        // create a Scanner from java util for input
        Scanner scanner = new Scanner(System.in);

        // create a menu bar
        System.out.println("MENU");
        System.out.println("=============");
        System.out.println("1. Identity");
        System.out.println("2. Calculator");
        System.out.println("3. Comparison");
        System.out.println();
        System.out.println("0. Exit");
        System.out.print("Input menu: ");

        // input menu you choose
        int menu = scanner.nextInt();
        scanner.nextLine();
        System.out.println();

        if(menu == 1) {
            // run the method from the menu you choose
            identity();
        } else if (menu == 2) {
            // run the method from the menu you choose
            calculator();
        } else if (menu == 3) {
            // run the method from the menu you choose
            comparison();
        } else if(menu == 0) {
            System.exit(0);
        } else {
            System.out.println("Invalid input\n");
            menu();
        }
    }

    public static void identity() {
        Scanner scanner = new Scanner(System.in);

        // print the menu
        System.out.println("IDENTITY");
        System.out.println("=============");
        System.out.println("What is your name?");
        // enter the string
        String name = scanner.nextLine();
        System.out.println("What is your reason to become a backend-developer? ");
        // enter the string
        String reason = scanner.nextLine();
        System.out.println("What is your expectation from this bootcamp? ");
        // enter the string
        String expectation = scanner.nextLine();
        System.out.println();

        // print the output from the string you've been entered
        System.out.println("Name\t\t\t\t\t: " + name);
        System.out.println("Reason to become backend-developer\t: " + reason);
        System.out.println("Expectation from this Bootcamp\t\t: " + expectation);
        System.out.println();

        System.out.println("9. Back");
        System.out.println("1. Restart");
        System.out.println("0. Exit");
        System.out.print("Input menu: ");
        int menu = scanner.nextInt();
        scanner.nextLine();
        System.out.println();

        if(menu == 9) {
            // recursive call
            menu();
        } else if(menu == 0) {
            System.exit(0);
        } else if(menu == 1) {
            // recursive call
            identity();
        } else {
            System.out.println("Invalid input\n");
            // recursive call
            menu();
        }
    }

    // create a calculator method
    public static void calculator() {
        Scanner scanner = new Scanner(System.in);

        // declare the result
        int result;

        // print the calculator's menu
        System.out.println("CALCULATOR");
        System.out.println("=============");
        System.out.println("1. Addition");
        System.out.println("2. Subtraction");
        System.out.println("3. Multiplication");
        System.out.println("4. Division");
        System.out.println("5. Modulo");
        System.out.println();
        System.out.println("9. Back");
        System.out.println("0. Exit");
        System.out.print("Input menu: ");
        int menu = scanner.nextInt();
        scanner.nextLine();
        System.out.println();

        // declare the numbers for the input
        int num1, num2;

        // create condition for every menu you input and create a recursive call
        if(menu == 1) {
            System.out.println("Addition");
            System.out.println("=============");
            System.out.print("Input number 1: ");
            num1 = scanner.nextInt();
            scanner.nextLine();
            System.out.print("Input number 2: ");
            num2 = scanner.nextInt();
            scanner.nextLine();
            result = num1 + num2;
            System.out.println();
            System.out.println("Result: " + result + "\n");
            calculatorFinishedMenu();
        } else if (menu == 2) {
            System.out.println("Subtraction");
            System.out.println("=============");
            System.out.print("Input number 1: ");
            num1 = scanner.nextInt();
            scanner.nextLine();
            System.out.print("Input number 2: ");
            num2 = scanner.nextInt();
            scanner.nextLine();
            result = num1 - num2;
            System.out.println();
            System.out.println("Result: " + result + "\n");
            calculatorFinishedMenu();
        } else if (menu == 3) {
            System.out.println("Multiplication");
            System.out.println("=============");
            System.out.print("Input number 1: ");
            num1 = scanner.nextInt();
            scanner.nextLine();
            System.out.print("Input number 2: ");
            num2 = scanner.nextInt();
            scanner.nextLine();
            result = num1 * num2;
            System.out.println();
            System.out.println("Result: " + result + "\n");
            calculatorFinishedMenu();
        } else if (menu == 4) {
            System.out.println("Division");
            System.out.println("=============");
            System.out.print("Input number 1: ");
            num1 = scanner.nextInt();
            scanner.nextLine();
            System.out.print("Input number 2: ");
            num2 = scanner.nextInt();
            scanner.nextLine();
            result = num1 / num2;
            System.out.println();
            System.out.println("Result: " + result + "\n");
            calculatorFinishedMenu();
        } else if (menu == 5) {
            System.out.println("Modulo");
            System.out.println("=============");
            System.out.print("Input number 1: ");
            num1 = scanner.nextInt();
            scanner.nextLine();
            System.out.print("Input number 2: ");
            num2 = scanner.nextInt();
            scanner.nextLine();
            result = num1 % num2;
            System.out.println();
            System.out.println("Result: " + result + "\n");
            calculatorFinishedMenu();
        } else if (menu == 9) {
            // recursive call
            menu();
        } else if (menu == 0) {
            // for exit the program
            System.exit(0);
        } else {
            System.out.println("Invalid input\n");
            calculator();
        }
    }

    // create the menu after the calculator finished so and apply the DRY method
    public static void calculatorFinishedMenu() {
        Scanner scanner = new Scanner(System.in);

        System.out.println("9. Back");
        System.out.println("0. Exit");
        System.out.print("Input menu: ");
        int menu = scanner.nextInt();
        scanner.nextLine();
        System.out.println();
        if(menu == 9) {
            // recursive call
            calculator();
        } else if (menu == 0) {
            System.exit(0);
        }
    }

    public static void comparison() {
        Scanner scanner = new Scanner(System.in);

        // print the comparison menu
        System.out.println("COMPARISON");
        System.out.println("=============");

        System.out.print("Input number 1: ");
        int num1 = scanner.nextInt();
        scanner.nextLine();
        System.out.print("Input number 2: ");
        int num2 = scanner.nextInt();
        scanner.nextLine();
        System.out.println();

        if(num1 < num2) {
            System.out.println(num1 + " is smaller than " + num2);
            System.out.println();
            // call the menu method
            comparisonMenu();
        } else if (num1 > num2) {
            System.out.println(num1 + " is greater than " + num2);
            System.out.println();
            // call the menu method
            comparisonMenu();
        } else {
            System.out.println("Both numbers are equals");
            System.out.println();
            // call the menu method
            comparisonMenu();
        }
    }

    // create a method for menu after the comparison method finished
    public static void comparisonMenu() {
        Scanner scanner = new Scanner(System.in);

        System.out.println("9. Back");
        System.out.println("1. Restart");
        System.out.println("0. Exit");
        System.out.print("Input menu: ");
        int menu = scanner.nextInt();
        scanner.nextLine();
        System.out.println();
        if(menu == 9) {
            menu();
        } else if(menu == 1) {
            comparison();
        } else if(menu == 0) {
            System.exit(0);
        } else {
            System.out.println("Invalid input\n");
            menu();
        }
    }
}
